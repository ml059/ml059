#include<stdio.h>

int main ()
{
    FILE *f1;
    int lower, upper, n, check;
    int check_num_in_range(int *low, int *up, int *x);
    char filename[20];
    
    printf("enter the file name : \n");
    scanf("%s", filename);
    
    printf("enter the lower and upper limit of the range : \n");
    scanf("%d", &lower);
    scanf("%d", &upper);
    
    printf("enter any number : \n");
    scanf("%d", &n);
    
    check = check_num_in_range(&lower, &upper, &n);
    printf("check = %d\n", check);
    
    if(check == 0)
    {
        printf("\ncannot open %s.\n", filename);
    }
    else if(check == 1)
    {
        f1 = fopen(filename, "w");
        fscanf(stdin, "%d", &n);
        fprintf(f1, "%d", n);
        
        fclose(f1);
        fprintf(stdout, "\n\n");
        
        f1 = fopen(filename, "r");
        printf("\nthe number is : \n");
        fscanf(f1, "%d", &n);
        fprintf(stdout, "%d", n);
        fclose(f1);
    }
    else
      printf("\nnothing\n");
}

int check_num_in_range(int *low, int *up, int *x)
{
    if(*x >= *low && *x <= *up)
    {
        printf("\nthe number is in range\n");
        return 1;
    }
    else
    {
        printf("\nthe number is not in range\n");
        return 0;
    }
}