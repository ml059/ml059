#include<stdio.h>

struct student
{
    int roll_no;
    char name[50];
    char sec[10];
    char dept[50];
    float fees;
    int marks;
};

int main()
{
    struct student std[100];
    int i,n,max;
    printf("enter the num of students : \n");
    scanf("%d",&n);
    printf("enter the details of students : \n");
    
    for(i=0;i<n;i++)
    {
        printf("enter roll number : \n");
        scanf("%d",&std[i].roll_no);
        printf("enter name : \n");
        scanf("%s",&std[i].name);
        printf("enter section : \n");
        scanf("%s",&std[i].sec);
        printf("enter department : \n");
        scanf("%s",&std[i].dept);
        printf("enter fees : \n");
        scanf("%f",&std[i].fees);
        printf("enter result : \n");
        scanf("%d",&std[i].marks);
    }
    
    for(i=0;i<n;i++)
    {
        max=std[0].marks;
        if(std[i].marks>max)
        {
            max=std[i].marks;
        }
    }
    for(i=0;i<n;i++)
    {
     if(std[i].marks==max)
     {
        printf("details of the student who scored highest marks\n");
        printf("roll number : %d\n",std[i].roll_no);
        printf("name : %s\n",std[i].name);
        printf("section : %s\n",std[i].sec);
        printf("department : %s\n",std[i].dept);
        printf("fees : %f\n",std[i].fees);
        printf("result : %d\n",std[i].marks);
     }
   }
    return 0;
}