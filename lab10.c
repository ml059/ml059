#include<stdio.h>

void main()
{
  int a,b;
  char operator;
  
  printf("enter the integers a and b : \n");
  scanf("%d",&a);
  scanf("%d",&b);
  printf("enter the operator : \n");
  scanf(" %c",&operator);
  
  arith(&a,&b,&operator);
  }
  
void arith(int *p1, int *p2, char *c)
{
  int result;
  switch (*c)
  {
    case '+':
           {
             result = *p1 + *p2;
             printf("the sum of two integers is : %d\n", result);
             break;
           }
    case '-':
           {
             result = *p1 - *p2;
             printf("the difference between two integers is : %d\n", result);
             break;
           }
    case '*':
           {
             result = (*p1) * (*p2);
             printf("the product of two integers is : %d\n", result);
             break;
           }
    case '/':
           {
             if(*p2==0)
             {
               printf("can not perform the operation with these inputs\n");
               break;
             }
             else
             {
               result = *p1 / *p2;
               printf("the quotient is : %d\n", result);
               break;
             }
           }
    case '%': 
           {
             if(*p2==0)
             {
               printf("can not perform the operation with these inputs\n");
               break;
             }
             else
             {
               result = *p1 % *p2;
               printf("the reminder is : %d\n", result);
               break;
             }
           }
    default:
            printf("invalid operator\n");
  }
}